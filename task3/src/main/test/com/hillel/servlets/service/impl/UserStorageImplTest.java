package com.hillel.servlets.service.impl;

import com.hillel.servlets.service.UserStorage;
import com.hillel.servlets.util.Util;
import org.junit.Before;
import org.junit.Test;

import static com.hillel.servlets.util.Constants.USER_NOT_LOGIN_STATUS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class UserStorageImplTest {
    private UserStorage userStorage;

    @Before
    public void setUp() {
        userStorage = new UserStorageImpl();
        Util.initUserStorage(userStorage);
    }

    @Test
    public void add() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void delete1() {
    }

    @Test
    public void contains() {
        boolean expected = true;
        String userName= "user1";
        boolean actual = userStorage.contains(userName);
        assertThat(expected, equalTo(actual));
    }

    @Test
    public void getUserStatus() {
        String expected = USER_NOT_LOGIN_STATUS;
        String userName= "user1";
        String actual = userStorage.getUserStatus(userName);
        assertThat(expected, equalTo(actual));
    }

    @Test
    public void setUserStatus() {
    }

    @Test
    public void getList() {
    }

    @Test
    public void getUser() {
    }

    @Test
    public void getPassword() {
    }
}