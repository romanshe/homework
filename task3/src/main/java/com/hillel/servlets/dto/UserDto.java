package com.hillel.servlets.dto;

import lombok.Data;
import com.hillel.servlets.model.User;

import java.util.List;


@Data
public class UserDto {
    private String firstName;
    private String lastName;
    private String userName;
    private String status;
    private List<RoleDto> roleDto;

}
