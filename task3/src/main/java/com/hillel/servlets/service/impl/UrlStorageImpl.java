package com.hillel.servlets.service.impl;

import com.hillel.servlets.service.UrlStorage;

import java.util.HashMap;
import java.util.Map;

public class UrlStorageImpl implements UrlStorage {
    private Map<String, String> storage = new HashMap<>();


    @Override
    public void addUrl(String userName, String url) {
        storage.put(userName, url);

    }

    @Override
    public Map<String, String> getUrlList() {
        return storage;
    }
}
