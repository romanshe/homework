package com.hillel.servlets.service;

import com.hillel.servlets.model.User;

import java.util.Collection;
import java.util.List;

public interface UserStorage {

    void add(User user);


    User delete(User user);
    User delete(String userName);

    boolean contains(String username);

    String getUserStatus(String username);

    void setUserStatus(String username, String status);

    List<User> getList();

    User getUser(String username);

    String getPassword(String username);
}
