package com.hillel.servlets.service.impl;

import com.hillel.servlets.dto.UserDto;
import com.hillel.servlets.model.User;
import com.hillel.servlets.util.Constants;
import com.hillel.servlets.service.UserService;
import com.hillel.servlets.service.UserStorage;
import com.hillel.servlets.util.Util;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;

import java.util.Collection;
import java.util.List;

public class UserServiceImpl implements UserService {
    private UserStorage storage;

    public UserServiceImpl(UserStorage storage){
        this.storage = storage;
    }

    @Override
    public boolean isExistingUser(String userName){
        if (StringUtils.isNotEmpty(userName))
            return storage.contains(userName);
        else
            return false;
    }

    @Override
    public boolean isCorrectPassword(String userName, String password){
        if (StringUtils.isNotEmpty(userName) && StringUtils.isNotEmpty(password))
            return storage.getPassword(userName).equals(password);
        else
            return false;
    }
    @Override
    public boolean changeUserName(String userName, String newUserName ){
        if (storage.contains(userName)) {
            storage.getUser(userName).setUserName(newUserName);
            return true;
        }
        return false;
    }
    @Override
    public boolean changeUserLastName(String userName, String newUserLastName ){
        return false;
    }
    @Override
    public boolean changeUserUserName(String userName, String newUserUserName ){
        return false;
    }

    @Override
    public boolean addUser(User user) {
        if (!storage.contains(user.getUserName())){
            storage.add(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteUser(String userName) {
        if (storage.contains(userName)){
            storage.delete(userName);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteUser(User user) {
        if (storage.contains(user.getUserName())){
            storage.delete(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean updateUser(User user) {
        if (isExistingUser(user.getUserName())){
            User updateUser = storage.getUser(user.getUserName());
            if (!user.getFirstName().isEmpty() && !user.getFirstName().equals(updateUser.getFirstName())){
                updateUser.setFirstName(user.getFirstName());
            }
            if (!user.getLastName().isEmpty() && !user.getLastName().equals(updateUser.getLastName())){
                updateUser.setLastName(user.getLastName());
            }
            return true;
        }
        return false;
    }

    @Override
    public User getUserByUserName(String userName){
        return storage.getUser(userName);
    }
    @Override
    public UserDto getUserInfoByUserName(String userName){
        return Util.convertToUserdDto(storage.getUser(userName));
    }


    @Override
    public User login(String userName, String password) {
        if (isExistingUser(userName) && isCorrectPassword(userName, password)) {
            User user = getUserByUserName(userName);
            storage.setUserStatus(userName, Constants.USER_LOGIN_STATUS);
            return user;
        } else {
            return null;
        }
    }

    @Override
    public boolean logout(User user) {
        return logout(user.getUserName());
    }

    @Override
    public boolean logout(String userName) {
        if (isExistingUser(userName)) {
            storage.setUserStatus(userName, Constants.USER_NOT_LOGIN_STATUS);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<User> getUserList() {
        return  storage.getList();
    }

    @Override
    public List<UserDto> getUserInfoList() {
        List<User> users = storage.getList();
        List<UserDto> userDtoList = new ArrayList();
        for (User user :users) {
            userDtoList.add(Util.convertToUserdDto(user));
        }
        return userDtoList;
    }

}
