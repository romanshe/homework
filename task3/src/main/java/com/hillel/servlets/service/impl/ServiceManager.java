package com.hillel.servlets.service.impl;

import com.hillel.servlets.model.Role;
import com.hillel.servlets.model.User;
import com.hillel.servlets.util.Constants;
import com.hillel.servlets.util.Util;
import com.hillel.servlets.service.UrlStorage;
import com.hillel.servlets.service.UserService;
import com.hillel.servlets.service.UserStorage;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;


public class ServiceManager {

    private static final String SERVICE_MANAGER = "SERVICE_MANAGER";
    private static Logger log = Logger.getLogger(ServiceManager.class);
    private final UserService userService;
    private final UserStorage userStorage;
    private final UrlStorage urlStorage;

    public ServiceManager() {
        this.userStorage = new UserStorageImpl();
        this.userService = new UserServiceImpl(userStorage);
        this.urlStorage = new UrlStorageImpl();
        initUserStorage(userStorage);
    }

    private static void initUserStorage(UserStorage userStorage) {
        userStorage.add(new User("User1","LastName1", "user1", "1", Constants.USER_NOT_LOGIN_STATUS, new Role("admin")));
        userStorage.add(new User("User2","LastName2", "user2", "1", Constants.USER_NOT_LOGIN_STATUS));
        userStorage.add(new User("User3","LastName3", "user3", "1", Constants.USER_NOT_LOGIN_STATUS));
        userStorage.add(new User("User4","LastName4", "user4", "1", Constants.USER_NOT_LOGIN_STATUS));
    }


    public static ServiceManager getInstance(ServletContext sc) {
        ServiceManager serviceManager = (ServiceManager) sc.getAttribute(SERVICE_MANAGER);
        return serviceManager;
    }

    public static void setInstance(ServletContext sc){
        ServiceManager serviceManager = new ServiceManager();
        sc.setAttribute(SERVICE_MANAGER, serviceManager);
    }

    public UserService getUserService() {
        return userService;
    }

    public UrlStorage getUrlStorage() {
        return urlStorage;
    }

    public void shutdown() {
        log.info("ServiceManager shutdown method");
    }
}
