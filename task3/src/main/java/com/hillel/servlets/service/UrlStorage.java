package com.hillel.servlets.service;

import java.util.*;

public interface UrlStorage {

    void addUrl(String userName, String url);
    Map<String, String > getUrlList();
}
