package com.hillel.servlets.service.impl;

import com.hillel.servlets.model.User;
import lombok.NoArgsConstructor;
import com.hillel.servlets.service.UserStorage;

import java.util.*;

@NoArgsConstructor
public class UserStorageImpl implements UserStorage {
    private List<User> storage = new ArrayList<>();

    @Override
    public void add(User user){
        storage.add(user);
    }

    @Override
    public User delete(User user){
        return storage.remove(storage.indexOf(user));
    }
    @Override
    public User delete(String userName){
        Iterator iterator = storage.iterator();
        while(iterator.hasNext()){
            User user = (User) iterator.next();
            if (user.getUserName().equals(userName)){
                iterator.remove();
                return user;
            }
        }
        return null;
    }
    @Override
    public boolean contains(String username){
        for (User user :storage) {
            if (user.getUserName().equals(username)){
                return true;
            }
        }
        return false;
    }
    @Override
    public String getUserStatus(String username){
        User user = getUser(username);
        return  user.getStatus();
    }
    @Override
    public void setUserStatus(String username, String status){
        User user = getUser(username);
        user.setStatus(status);
    }
    @Override
    public List<User> getList(){
        return new ArrayList<User>(storage);
    }
    @Override
    public User getUser(String username){
        for (User user :
                storage) {
            if (user.getUserName().equals(username)){
                return user;
            }
        }
        return null;
    }
    @Override
    public String getPassword(String username){
        User user = getUser(username);
        return user.getPassword();
    }
}
