package com.hillel.servlets.service;

import com.hillel.servlets.dto.UserDto;
import com.hillel.servlets.model.User;

import java.util.Collection;


public interface UserService {
    User login(String userName, String password);


    boolean isExistingUser(String userName);

    boolean isCorrectPassword(String userName, String password);

    boolean changeUserName(String userName, String newUserName );

    boolean changeUserLastName(String userName, String newUserLastName );

    boolean changeUserUserName(String userName, String newUserUserName );

    boolean addUser(User user);
    boolean deleteUser(User userName);
    boolean deleteUser(String userName);
    boolean updateUser(User user);

    User getUserByUserName(String userName);
    UserDto getUserInfoByUserName(String userName);

    boolean logout(User user);
    boolean logout(String userName);

    Collection<User> getUserList();
    Collection<UserDto> getUserInfoList();
}
