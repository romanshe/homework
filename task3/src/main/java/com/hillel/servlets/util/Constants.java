package com.hillel.servlets.util;

public class Constants {

    public static final String CONTEXT = "";

    // ATTRIBUTES
    public static final String ATTRIBUTE_USER_DTO = "user";

    //
    public static final String USER_LOGIN_STATUS = "Logged in";
    public static final String USER_NOT_LOGIN_STATUS = "Not logged in";
    public static final String USER_NAME = "userName";

    // PAGES
    public static final String PAGE_MAIN = CONTEXT + "/WEB-INF/index.jsp";
    public static final String PAGE_LOGIN = CONTEXT + "/WEB-INF/Login.jsp";
    public static final String PAGE_ADMIN = CONTEXT + "/WEB-INF/Admin.jsp";
    public static final String PAGE_UPDATE = CONTEXT + "/WEB-INF/Update.jsp";
    public static final String PAGE_HOME = CONTEXT + "/WEB-INF/Home.jsp";
    public static final String PAGE_USER_LIST = CONTEXT + "/WEB-INF/Users.jsp";


}
