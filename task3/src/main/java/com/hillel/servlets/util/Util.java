package com.hillel.servlets.util;

import com.hillel.servlets.dto.RoleDto;
import com.hillel.servlets.dto.UserDto;
import com.hillel.servlets.model.Role;
import com.hillel.servlets.model.User;
import com.hillel.servlets.service.UserStorage;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.graalvm.compiler.lir.LIRInstruction;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Util {
    private static Logger log = Logger.getLogger(Util.class);

    public static User parseJsonInUser(ServletInputStream stream) throws IOException {

        log.info("parse JSON in user");

        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder stringBuilder = new StringBuilder();
        String json = "";
        if (br != null) {
            while ((json = br.readLine()) != null) {
                stringBuilder.append(json);
            }
        }
        Object obj = null;
        try {
            obj = new JSONParser().parse(stringBuilder.toString());
        } catch (ParseException e) {
            log.warn("Exception in parsing Json to User");
        }
        JSONObject jo = (JSONObject) obj;

        String userName = (String) jo.get("userName");
        String firstName = (String) jo.get("firstName");
        String lastName = (String) jo.get("lastName");
        String password = (String) jo.get("password");

        if (StringUtils.isEmpty(userName)) {
            log.warn("Cannot find user in stream");
            return null;
        } else {
            log.info("UserDto was found in stream");
            return new User(firstName, lastName, userName, password, Constants.USER_NOT_LOGIN_STATUS);
        }
    }


    public static UserDto convertToUserdDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setStatus(user.getStatus());
        userDto.setUserName(user.getUserName());
        List<RoleDto> roleDtoList = new ArrayList();
        for (Role role : user.getRole()) {
            roleDtoList.add(convertRoleToRoleDto(role));
        }
//        user.getRole().stream().forEach(Util::convertRoleToRoleDto);
        userDto.setRoleDto(roleDtoList);
        return userDto;
    }

    public static RoleDto convertRoleToRoleDto(Role role) {
        return new RoleDto(role.getName());
    }

}
