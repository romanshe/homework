package com.hillel.servlets.filter;

import com.hillel.servlets.dto.RoleDto;
import com.hillel.servlets.dto.UserDto;
import com.hillel.servlets.model.Role;
import com.hillel.servlets.service.impl.ServiceManager;
import com.hillel.servlets.servlet.AbstractServlet;
import org.apache.log4j.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.hillel.servlets.util.Constants.ATTRIBUTE_USER_DTO;


@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/*")
public class AuthenticationFilter extends AbstractFilter {
    public static Logger log = Logger.getLogger(AuthenticationFilter.class);
    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {

        log.info("AuthenticationFilter doFilter");
        UserDto userDto = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER_DTO);
        String currentUri = req.getRequestURI();
        if (userDto == null) {
            if (currentUri.endsWith("/login") || currentUri.contains("/static")|| currentUri.contains("/management/user") && req.getMethod().equals("POST") ) {
                chain.doFilter(req, resp);
            } else {
                HttpSession session = req.getSession();
                if (session != null)
                    ServiceManager.getInstance(session.getServletContext()).getUrlStorage().addUrl("no user", currentUri);

                resp.sendRedirect(req.getContextPath() + "/user/login");
            }
        } else {
            if (currentUri.endsWith("/admin") || currentUri.contains("/management")) {
                if (userDto.getRoleDto().contains(new RoleDto("admin"))) {
                    chain.doFilter(req, resp);
                } else {
                    resp.sendRedirect(req.getContextPath() + "/user/login");
                    ServiceManager.getInstance(req.getServletContext()).getUrlStorage().addUrl(userDto.getUserName(), currentUri);
                }
            } else {
                chain.doFilter(req, resp);
            }
        }
    }
}
