package com.hillel.servlets.servlet;

import com.hillel.servlets.dto.UserDto;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.hillel.servlets.util.Constants.ATTRIBUTE_USER_DTO;

@WebServlet("/user/logout")
public class UserLogoutServlet extends AbstractServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("/user/logout doGet");

        UserDto userDto = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER_DTO);
        getServiceManager().getUserService().logout(userDto.getUserName());
        req.getSession().invalidate();
        resp.sendRedirect("/servlets/user/login");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("/user/logout doPut");

        UserDto userDto = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER_DTO);
        getServiceManager().getUserService().logout(userDto.getUserName());

        req.getSession().invalidate();
        resp.sendRedirect("/servlets/user/login");
    }
}
