package com.hillel.servlets.servlet;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.hillel.servlets.util.Constants.*;

@WebServlet("/user/home")
public class UserHomeServlet extends AbstractServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("/user/home doGet");
        req.setAttribute("users", getServiceManager().getUserService().getUserList());

        req.getRequestDispatcher(PAGE_HOME).forward(req, resp);
    }
}
