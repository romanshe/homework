package com.hillel.servlets.servlet;


import com.hillel.servlets.dto.UserDto;
import com.hillel.servlets.model.User;
import com.hillel.servlets.util.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



import static com.hillel.servlets.util.Constants.*;


@WebServlet(urlPatterns = "/user/login", name = "UserLoginServlet")
public class UserLoginServlet extends AbstractServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("/user/login Method doGet");
        req.getRequestDispatcher(PAGE_LOGIN).forward(req, resp);
}

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("/user/login Method doPost");

        String userName = req.getParameter("username");
        String password = req.getParameter("password");

        User user = getServiceManager().getUserService().login(userName, password);
        if (user != null) {
            log.info("User logged successful");
            UserDto userDto = Util.convertToUserdDto(user);
            req.getSession().setAttribute(ATTRIBUTE_USER_DTO, userDto);
            resp.sendRedirect("/servlets/user/home");
        } else {
            log.info("Wrong username or password");
            req.getRequestDispatcher(PAGE_LOGIN).forward(req, resp);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) {
        log.info("/user/login doPut");
    }
}
