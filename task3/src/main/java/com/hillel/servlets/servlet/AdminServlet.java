package com.hillel.servlets.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.hillel.servlets.util.Constants.*;

@WebServlet("/admin")
public class AdminServlet extends AbstractServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("/admin doGet");
        req.getRequestDispatcher(PAGE_ADMIN).forward(req, resp);
    }
}
