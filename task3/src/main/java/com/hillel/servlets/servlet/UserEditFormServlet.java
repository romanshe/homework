package com.hillel.servlets.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.hillel.servlets.util.Constants.*;

@WebServlet("/user/update")
public class UserEditFormServlet extends AbstractServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("user/update doGet");

        req.getSession().setAttribute(USER_NAME, req.getParameter("userName"));
        req.getRequestDispatcher(PAGE_UPDATE).forward(req, resp);
    }
}
