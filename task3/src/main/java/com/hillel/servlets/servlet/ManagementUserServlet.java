package com.hillel.servlets.servlet;


import com.google.gson.Gson;
import com.hillel.servlets.model.User;
import com.hillel.servlets.util.Util;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/management/user")
public class ManagementUserServlet extends AbstractServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("/management/user doGet");
        String userName = req.getParameter("userName");
        Gson gson = new Gson();
        String userInfo = gson.toJson(getServiceManager().getUserService().getUserInfoByUserName(userName));
        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.print(userInfo);
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("/management/user doPost");

        User user = Util.parseJsonInUser(req.getInputStream());
        if (user == null){
            log.warn("There are no user in request");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "There are no user in request");
        } else {
            if (getServiceManager().getUserService().addUser(user)) {
                log.info("Add user to the storage " + user.getUserName());
            } else {
                log.warn("This user already exist: " + user.getUserName());
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "This user already exist");
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("/management/user doPut");

        User user = Util.parseJsonInUser(req.getInputStream());

        if (getServiceManager().getUserService().updateUser(user)){
            log.info("Change user: " + user.getUserName());
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find user");
            log.warn("Cannot find user: " + user.getUserName());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.info("/management/user doDelete");

        String userName = req.getParameter("username");

        if (getServiceManager().getUserService().deleteUser(userName)){

            log.info("Delete user: " + userName);
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Cannot find user");
            log.warn("Cannot find user: "  +userName);
        }
    }



}
