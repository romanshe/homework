package com.hillel.servlets.servlet;

import com.google.gson.Gson;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/users")
public class UsersServlet extends AbstractServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("Method doGet");
        Gson gson = new Gson();

        String userList = gson.toJson(getServiceManager().getUserService().getUserInfoList());
        log.info("Convert storage to Json");

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(userList);
        out.flush();
        log.info("Send user-list");
    }



}
