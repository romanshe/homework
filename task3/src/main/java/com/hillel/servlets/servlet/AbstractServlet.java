package com.hillel.servlets.servlet;

import com.hillel.servlets.service.impl.ServiceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;

public class AbstractServlet extends HttpServlet {
    public static Logger log = Logger.getLogger(AbstractServlet.class);
    private ServiceManager serviceManager;

    @Override
    public void init() {
        serviceManager = ServiceManager.getInstance(getServletContext());
        log.info("init AbstractServlet");

    }

    public ServiceManager getServiceManager() {
        return serviceManager;
    }
}
