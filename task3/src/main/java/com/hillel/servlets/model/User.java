package com.hillel.servlets.model;

import lombok.Data;
import java.util.List;
import java.util.ArrayList;



@Data
public class User {
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String status;
    private List<Role> role;

    public User(String firstName, String lastName, String userName, String password, String status, Role role){
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.role = new ArrayList();
        Role user = new Role("user");
        this.role.add(user);
        this.role.add(role);

    }

    public User(String firstName, String lastName, String userName, String password, String status){
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.role = new ArrayList();
        Role user = new Role("user");
        this.role.add(user);
    }
}
