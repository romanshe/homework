package com.hillel.servlets.model;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Role {
    private String name;

    @Override
    public String toString() {
        return name;
    }
}
