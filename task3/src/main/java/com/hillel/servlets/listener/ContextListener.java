package com.hillel.servlets.listener;

import com.hillel.servlets.service.impl.ServiceManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {
    private static Logger log = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("Context Listener init");
        ServiceManager.setInstance(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Context Listener destroy");
        ServiceManager.getInstance(sce.getServletContext()).shutdown();
    }
}
