<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<link rel="stylesheet" href="/servlets/static/css.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="/servlets/static/my_js.js"></script>
	</head>
	<body>
	<div class="user-page">	
	  <div class="form">
		<form class="login-form" >
			<div>
				<input type="text" class='userName' placeholder="" />
				<input type="text" class='firstName' placeholder="" />
				<input type="text" class='lastName' placeholder="" />
				<a class="nav-link" href="/servlets/admin">Admin page</a>
				<a class="nav-link updateUser" href="#">Save chenges</a>
			</div>
		</form>
	  </div>
	</div>

	</body>
	<script>
		$(document).ready(function(){
			console.log('func');
			$.ajax({
			  url: '/servlets/management/user?userName=' + '${userName}',
			  type: 'GET',
			  success: function(data,status)
			  {
				console.log('data is recived: ' + data.userName);
				$(".userName").val(data.userName);
				$(".firstName").val(data.firstName);
				$(".lastName").val(data.lastName);
			  },
			  dataType: 'json'
			});	
		});
		
		$(".updateUser").click(function(event){
			event.preventDefault();
			console.log('update user');	
			 var dataObject = 	{
						"firstName": $(".firstName").val(),
						"lastName": $(".lastName").val(),
						"userName": $(".username").val()
					};
			$.ajax({
			  url: '/servlets/management/user?username=' + '${userName}',
			  type: 'PUT',
			  contentType: "application/json",
			  data: JSON.stringify(dataObject),
			  success: function(data,status)
			  {
				console.log('addUser ok ');
			  }	
			});
		});
	</script>
</html>