<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<link rel="stylesheet" href="/servlets/static/css.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="/servlets/static/my_js.js"></script>
	</head>
	<body>
		<div class="user-page">	
		  <div class="form">
			<form class="login-form" >
				<h2>Success login</h2>
				<div class="row justify-content-center">
					<label> User name:  ${user.userName} </label>
				</div>
				<div class="row justify-content-center">
					<label> First name:  ${user.firstName} </label>
				</div>
				<div class="row justify-content-center">
					<label> Last name:  ${user.lastName} </label>
				</div>
				<div class="row">
					<a class="nav-link" href="/servlets/admin">Admin page</a>
				</div>
				<div class="row">
					<a class="nav-link" href="/servlets/user/logout">Logout</a>
				</div>
			</form>
		  </div>
		</div>
	</body>
	<script>
		$(document).ready(function(){
			$(".logout").click(function(){
				$.ajax({
				  url: '/servlets/user/logout',
				  type: 'PUT',
				  success: function(data,status)
				  {
					console.log('logout ');
				  }
				});	
			});
			
		});
	</script>
</html>