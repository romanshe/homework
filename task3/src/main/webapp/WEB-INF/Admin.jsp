<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<link rel="stylesheet" href="/servlets/static/css.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="/servlets/static/my_js.js" ></script>
	</head>
	<body>	
		<div class="admin-page">	
		  <div class="admin-form">
			<table class="MyTable"></table>
			<div class="row">
				<div class="row">
					<a class="nav-link reload-table" >Reload table</a>
					<a class="nav-link"  href="/servlets/user/home"  >Home</a>
				</div>
				<div>
					
				</div>
			</div>
		  </div>
		</div>
	</body>
</html>