<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/servlets/static/css.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	</head>
	<body>
		<div class="login-page">
		  <div class="form">
			<form class="register-form" action='/servlets/user/register' method='post'>
			  <input type="text" class='username' placeholder="user name" required=''/>
			  <input type="password" class='password' placeholder="password" required=''/>
			  <input type="password" class='confirmPassword' placeholder="Confirm password" required=''/>
			  <input type="text" class='firstName' placeholder="first name" required=''/>
			  <input type="text" class='lastName' placeholder="last name"/>
			  <button class="addUser">create</button>
			  <p class="message">Already registered? <a href="#">Sign In</a></p>
			</form>
			<form class="login-form" action='/servlets/user/login' method='post'>
			  <input type="text" name='username' placeholder="username"/>
			  <input type="password" name='password' placeholder="password"/>
			  <button type="submit">login</button>
			  <p class="message">Not registered? <a href="#">Create an account</a></p>
			</form>
		  </div>
		</div>

	</body>
	<script>
		$(document).ready(function(){
			$('.message a').click(function(){
			   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
			});

			$(".addUser").click(function(event){
				event.preventDefault();
				console.log('addUser');
				 var dataObject = 	{
										"firstName": $(".firstName").val(),
										"lastName": $(".lastName").val(),
										"userName": $(".username").val(),
										"password": $(".password").val()
									};
				console.log(dataObject);
				$.ajax({
				  url: '/servlets/management/user',
				  type: 'POST',
				  contentType: "application/json",
				  data: JSON.stringify(dataObject),
				  success: function(data,status)
				  {
					console.log('addUser ok ');
				  }				  
				});	
			});
		});

	</script>
</html>


