package mr.she;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Plain {
    private @Setter
    String title;
    private @Getter
    int id;
}
