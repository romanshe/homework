package mr.she;


public class UserImpl implements User {
    private int userId;

    public UserImpl(int userId) {
        this.userId = userId;
    }

    @Override
    public int geuUserId() {
        return userId;
    }

    @Override
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String doubleString(String string) {
        return string + string;
    }
}
